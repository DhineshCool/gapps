# Recorder
PRODUCT_PACKAGES += \
    arcore \
    com.google.android.apps.dialer.call_recording_audio.features \
    RecorderPrebuilt

# Live Wallpapers
ifeq ($(TARGET_INCLUDE_LIVE_WALLPAPERS),true)
PRODUCT_PACKAGES += \
    PixelLiveWallpaperPrebuilt
endif

# Pixel Dependencies
PRODUCT_PACKAGES += \
    PixelDependencies

# Wallpapers
PRODUCT_PACKAGES += \
    WallpaperEmojiPrebuilt
